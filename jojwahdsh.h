/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   jojwahdsh.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlamhidr <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/05 21:27:03 by hlamhidr          #+#    #+#             */
/*   Updated: 2019/03/07 17:15:41 by hlamhidr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef JOJWAHDSH_H
# define JOJWAHDSH_H

# include <unistd.h>
# include <stdio.h>
# include <sys/wait.h>
# include <dirent.h>
# include <sys/stat.h>
# include <string.h>
# include <fcntl.h>
# include "libft/libft.h"
# include <termios.h>
# include <term.h>
# include <stdlib.h>
# include <curses.h>
# include <sys/ioctl.h>
# define MAX_HISTORY 20
# define UP(x) (x[0] == 27 && x[1] == '[' && x[2] == 'A')
# define DO(x) (x[0] == 27 && x[1] == '[' && x[2] == 'B')
# define RI(x) (x[0] == 27 && x[1] == '[' && x[2] == 'C')
# define LE(x) (x[0] == 27 && x[1] == '[' && x[2] == 'D')
# define SEL_RI(x) (x[0] == -30 && x[1] == -119 && x[2] == -91)
# define SEL_LE(x) (x[0] == -30 && x[1] == -119 && x[2] == -92)
# define COPY(x) (x[0] == -61 && x[1] == -89 && x[2] == 0)
# define PASTE(x) (x[0] == -30 && x[1] == -120 && x[2] == -102)
# define CUT(x) (x[0] == -30 && x[1] == -119 && x[2] == -120)
# define LE_WOR(x) (x[0] == -59 && x[1] == -109 && x[2] == 0)
# define RI_WOR(x) (x[0] == -30 && x[1] == -120 && x[2] == -111)
# define HOME(x) (x[0] == 27 && x[1] == 91 && x[2] == 72)
# define END(x) (x[0] == 27 && x[1] == 91 && x[2] == 70)
# define C_UP(x) (x[0] == 27 && x[1] == 91 && x[2] == 49 && x[3] == 59 && x[4] == 53 && x[5] == 65)
# define C_DO(x) (x[0] == 27 && x[1] == 91 && x[2] == 49 && x[3] == 59 && x[4] == 53 && x[5] == 66)

typedef struct	s_env
{
	char			*key;
	char			*value;
	struct s_env	*next;
}				t_env;

typedef struct	s_cursor
{
	int	index;
	int x;
	int y;
	int *end;
	FILE *fd;
}				t_cursor;

typedef	struct	s_select
{
	int start;
	int end;
	char *save;
}				t_select;

typedef struct	s_commandinfo
{
	char		***pcmd;
	int			out;
	int			in;
	int			pipe;
}				t_ci;

int		g_sign;

size_t			ft_lstlen(t_env *list);
int				ft_indexofchar(char *str, int c);
t_env			*ft_lstenvnew(char *key, char *value);
t_env			*add_last(t_env *lst, t_env *new);
t_env			*env_management(char **env);
char			*ft_read_line(char **history, int his_count, t_select *select);
char			**ft_split_command(char *line);
char			*ft_get_value(char *variable, t_env *e);
char			**ft_get_paths(t_env *e);
char			*ft_find_command_path(char *command, t_env *e, int j);
char			**ft_convert_totab(t_env *e);
void			ft_free_tab(char **tb);
void			ft_exit_and_free(char **tb, char *path, char *home, t_env *e);
void			ft_eddit_tab(char ***tb, char *home, t_env *e);
void			ft_minishell_exec(char **tb, char *path, t_env *e);
int				ft_tablen(char **tb);
int				ft_valid_line(char *line);
void			ft_setenv(char **tb, t_env *e);
int				ft_check_variables(char **tb, t_env *e);
t_env			*ft_unsetenv(char **tb, t_env *e);
void			ft_putvariable(char *variable, t_env *e);
void			ft_echo(char **tb);
void			ft_callincenter(char **tb, char *path, char *home, t_env *e);
int				ft_check_cd(char **tb);
void			ft_cd(char **tb, char *home, t_env *e);
void			ft_remove_lst(t_env *e);
void			ft_replace_value(char *key, char *value, t_env **e);
void			ft_putvariable(char *variable, t_env *e);
void			ft_catch_signal(int signal);
int				ft_numberof(char *str, int c);
void			ft_cd(char **tb, char *home, t_env *e);
void			ft_remove_last(char **s, int c);
void			ft_see_touch(char t, char *s, t_cursor *pos, t_select *select);
int				ft_get_size_windz(void);
int				ft_set_termcap(void);
int				ft_get_y_position(void);
void			ft_stock_history(char **history, char *line, int his_count);
int				my_outc(int c);
void			ft_selection(char *s, t_cursor *pos, char *key, t_select *select);
int				ft_get_num_of_lines_add(int len, int num_col);
void			ft_move_left(int n);
void			ft_set_last_position_for_add(t_cursor pos, int real_pos, int num_lines, int num_col);
void			ft_movecur_up_and_right(int up, int size);
int				ft_get_num_of_lines(int num_col, char *s);
void			ft_set_last_position(t_cursor pos, int num_lines, int num_col);
void			ft_move_cursor_zero(t_cursor pos);
void			ft_set_last_position_right_select(t_cursor pos, int num_lines, int real_pos, int len);
void			ft_copy_paste(char *buf, char **s, t_cursor *pos, t_select *select);
void			ft_mmmm(int **d);
void			ft_putstr_term(int num_col, char *s, t_cursor *pos);
void			ft_get_end_of_line_pos(t_cursor *pos, char *s, int num_col);
void			ft_get_new_pos(t_cursor *pos, char *s, int len_sa);
void			ft_move_by_word(t_cursor *pos, char *s, char *buf);
void			ft_home_end(t_cursor *pos, char *s, char *buf);
void			ft_move_by_lines(t_cursor *pos, char *s, char *buf);

#endif

