/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   termcap.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlamhidr <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/05 17:19:50 by hlamhidr          #+#    #+#             */
/*   Updated: 2019/04/05 17:19:51 by hlamhidr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "jojwahdsh.h"

int		ft_set_termcap()
{
	char *term_name;
	struct termios term;

	if (!(term_name = getenv("TERM")))
		return (-1);
	if (tgetent(NULL, term_name) == ERR)
		return (-1);
	if (tcgetattr(0, &term) == ERR)
		return (-1);
	term.c_lflag &= ~(ICANON);
	term.c_lflag &= ~(ECHO);
	term.c_cc[VMIN] = 1;
	term.c_cc[VTIME] = 0;
	if (tcsetattr(0, TCSANOW, &term) == ERR)
		return (-1);
	return (0);
}

int ft_get_y_position()
{
	struct termios term, restore;
	char buf[30];
	int i;
	int y;
	int pow;
	char ch;

	y = 0;
	tcgetattr(0, &term);
	tcgetattr(0, &restore);
	term.c_lflag &= ~(ICANON|ECHO);
	tcsetattr(0, TCSANOW, &term);
	write(1, "\033[6n", 4);
	i = 0;
	ch = 0;
	while (ch != 'R')
	{
		read(0, &ch, 1);
		buf[i++] = ch;
	}
	if (i < 2)
		return(1);
	i -= 2;
	while(buf[i] != ';')
		i--;
	i--;
	pow = 1;
	while(buf[i] != '[')
	{
		y = y + ( buf[i--] - '0' ) * pow;
		pow *= 10;
	}
	tcsetattr(0, TCSANOW, &restore);
	return (y - 1);
}
