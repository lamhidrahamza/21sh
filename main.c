/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlamhidr <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/03 14:28:33 by hlamhidr          #+#    #+#             */
/*   Updated: 2019/03/09 16:55:18 by hlamhidr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "jojwahdsh.h"

void	ft_minishell_exec(char **tb, char *path, t_env *e)
{
	int		child;
	char	**env;

	env = ft_convert_totab(e);
	child = fork();
	if (!child)
	{
		if (!path)
		{
			ft_putstr("21sh: command not found: ");
			ft_putstr(tb[0]);
			ft_putchar('\n');
		}
		else
		{
			execve(path, tb, env);
			ft_putstr("permission denied\n");
		}
		exit(0);
	}
	g_sign = 1;
	wait(0);
	g_sign = 0;
	ft_free_tab(env);
}

char	*ft_path_of_command(char *command, t_env *e)
{
	struct stat	mystat;
	char		*path;

	path = ft_find_command_path(command, e, 0);
	if (!path && stat(command, &mystat) == 0)
		path = ft_strdup(command);
	return (path);
}

int		ft_management(t_env *e, char *home, char *line, char **tb)
{
	char *path;

	if (!ft_strlen(line) || !ft_valid_line(line))
		return (1);
	if (!ft_check_variables(tb, e))
		return (1);
	ft_eddit_tab(&tb, home, e);
	path = ft_path_of_command(tb[0], e);
	ft_callincenter(tb, path, home, e);
	return (0);
}

int		ft_find_pipe(t_env *e, char *home, char *cmd, t_ci c)
{
	int		tmp[2];
	int		fdr[2];
	int		fdp[2];
	int		j;
 
	j = 0;
	tmp[0] = dup(0);
	tmp[1] = dup(1);
	if (c.in != -1)
		fdr[0] = c.in;
	else
		fdr[0] = dup(0);
	fdr[1] = dup(1);
	while (c.pcmd[j])
	{
		dup2(fdr[0], 0);
		close(fdr[0]);
		if (c.pcmd[j + 1])
		{
			pipe(fdp);
			fdr[0] = fdp[0];
			fdr[1] = fdp[1];
		}
		else
		{
			if (c.out != -1)
				fdr[1] = c.out;
			else
			{	
				fdr[0] = tmp[0];
				fdr[1] = tmp[1];
			}
		}
		dup2(fdr[1], 1);
		
		ft_management(e, home, cmd, c.pcmd[j]);
		close(fdr[1]);
		j++;
	}
	dup2(tmp[0], 0);
	dup2(tmp[1], 1);
	close(tmp[0]);
	close(tmp[1]);
	return (0);
}

void	ft_removefrom(char **line, char c)
{
	int i;

	i = 0;
	while ((*line)[i])
	{
		if ((*line)[i] == c)
			break ;
		i++;
	}
	while ((*line)[i])
	{
		(*line)[i] = '\0';
		i++;
	}
}

int		ft_numberofpipes(char *line)
{
	int i;
	int j;

	i = 0;
	j = 0;
	while (line[i])
	{
		if (line[i] == '|')
			j++;
		i++;
	}
	return (j);
}

int		ft_appended_output(char **line)
{
	char *tmp;
	int out;
	int i;

	tmp = NULL;
	out = -1;
	i = ft_strlen(*line) - 1;
	while (i > 0)
	{
		if ((*line)[i] == '>' && (*line)[i - 1] == '>')
			tmp = ft_strtrim(ft_strrchr(*line, '>') + 1);
		i--;
	}
	if (tmp)
	{
		ft_removefrom(line, '>');
		out = open(tmp, O_RDWR | O_CREAT | O_APPEND, 0644);
	}
	return (out);
}

t_ci	ft_pipe_redirection(char *line)
{
	t_ci	ci;
	int		n;
	char	*outfile = NULL;
	char	*infile = NULL;
	char	**tb;
	int		j;

	n = ft_numberofpipes(line);
	ci.pcmd = (char ***)malloc(sizeof(char **) * n + 2);
	ci.pipe = 0;
	ci.out = -1;
	ci.in = -1;
	j = 0;
	tb = NULL;
	if (ft_strchr(line, '<'))
		infile = ft_strtrim(ft_strchr(line, '<') + 1);
	ci.out = ft_appended_output(&line);
	if (ft_strchr(line, '>'))
		outfile = ft_strtrim(ft_strrchr(line, '>') + 1);
	if (infile)
	{
		ft_removefrom(&line, '<');
		ci.in = open(infile, O_RDONLY);
	}
	if (outfile)
	{
		ft_removefrom(&line, '>');
		ci.out = open(outfile, O_RDWR | O_CREAT, 0644);
	}
	if (n)
	{
		ci.pipe = 1;
		tb = ft_strsplit(line, '|');
	}
	n = 0;
	if (!tb)
	{
		ci.pcmd[0] = ft_split_command(line);
		ci.pcmd[1] = NULL;
	}
	else
	{
		while (tb[n])
			ci.pcmd[j++] = ft_split_command(tb[n++]);
		ci.pcmd[j] = NULL;
	}
	return (ci);
}

int		ft_commands_management(char *home, t_env *e, char *line)
{
	int		i;
	char	**tb;

	i = ft_numberof(line, ';');
	tb = NULL;
	if (i == 0)
		return (ft_find_pipe(e, home, line, ft_pipe_redirection(ft_strdup(line))));
	else
	{
		tb = ft_strsplit(line, ';');
		free(line);
	}
	i = 0;
	while (tb[i])
	{
		if (ft_find_pipe(e, home, tb[i], ft_pipe_redirection(ft_strdup(tb[i]))))
			return (1);
		i++;
	}
	if (tb)
		free(tb);
	return (0);
}

void	ft_stock_history(char **history, char *line, int his_count)
{
	int i;

	i = 1;
	if (his_count < MAX_HISTORY)
	{
	//	free(history[his_count]);
		history[his_count] = ft_strdup(line);
	}
	else
	{
		free(history[0]);
		while (i < MAX_HISTORY)
		{	
			history[i - 1] = history[i];
			i++;
		}
		history[MAX_HISTORY - 1] = ft_strdup(line);
	}
}

char	**ft_alloc_tab(void)
{
	int i;
	char **tb;

	i = 0;
	tb = (char **)malloc(sizeof(char *) * MAX_HISTORY + 1);
	while (i < MAX_HISTORY)
		tb[i++] = NULL;
	tb[i] = 0;
	return (tb);
}

int		main(int ac, char **av, char **env)
{
	char	*line;
	char	*home;
	char	**history;
	int		his_count = 0;
	t_select	select;
	t_env	*e;

	av = NULL;
	g_sign = 0;
	select.start = -1;
	select.end = -1;
	select.save = NULL;
	history = ft_alloc_tab();
	signal(SIGINT, ft_catch_signal);
	e = env_management(env);
	home = ft_strdup(ft_get_value("HOME", e));
	ft_set_termcap();
	if (ac == 1)
		while (1)
		{
			ft_putstr("\033[0;32m21sh $>\033[0m ");
			line = ft_read_line(history, his_count, &select);
			if (ft_strcmp(line, ""))
			{	
				ft_stock_history(history, line, his_count);
				if (his_count < MAX_HISTORY)
					his_count++;
			}
			if (ft_commands_management(home, e, line))
				continue ;
		}
	ft_remove_lst(e);
	free(home);
	return (0);
}
