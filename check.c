/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlamhidr <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/05 21:39:41 by hlamhidr          #+#    #+#             */
/*   Updated: 2019/03/07 16:43:02 by hlamhidr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "jojwahdsh.h"

void	ft_puttwo(char *s1, char *s2)
{
	ft_putstr(s1);
	ft_putstr(s2);
}

int		ft_check_cd(char **tb)
{
	DIR *dir;

	if (ft_tablen(tb) > 2)
	{
		ft_putstr("cd: Too many arguments.\n");
		return (0);
	}
	if (access(tb[1], F_OK) == -1)
	{
		ft_puttwo(tb[1], ": No such file or directory.\n");
		return (0);
	}
	if ((dir = opendir(tb[1])) == NULL)
	{
		ft_puttwo(tb[1], ": Not a directory.\n");
		return (0);
	}
	else
		closedir(dir);
	if (access(tb[1], X_OK) == -1)
	{
		ft_puttwo(tb[1], ": Permission denied.\n");
		return (0);
	}
	return (1);
}

int		ft_check_variables(char **tb, t_env *e)
{
	int j;

	j = 0;
	while (tb[j])
	{
		if (tb[j][0] == '$' && !ft_get_value(tb[j] + 1, e))
		{
			ft_putstr(tb[j] + 1);
			ft_putstr(": Undefined variable.\n");
			return (0);
		}
		j++;
	}
	return (1);
}

int		ft_valid_line(char *line)
{
	int i;
	int n;

	i = 0;
	n = 0;
	while (line[i])
	{
		if (line[i] == '"' || line[i] == '\'')
			n++;
		i++;
	}
	if (n % 2 != 0)
	{
		ft_putstr("Unmatched \".\n");
		return (0);
	}
	return (1);
}
