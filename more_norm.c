/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   more_norm.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlamhidr <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/07 17:00:26 by hlamhidr          #+#    #+#             */
/*   Updated: 2019/03/07 17:14:18 by hlamhidr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "jojwahdsh.h"

int		ft_indexofchar(char *str, int c)
{
	int i;

	i = 0;
	while (str[i])
	{
		if (str[i] == c)
			return (i);
		i++;
	}
	return (-1);
}

int		ft_numberof(char *str, int c)
{
	int j;
	int i;
	int n;

	j = 0;
	i = 0;
	n = 0;
	while (str[i])
	{
		if (str[i] == '\"')
			j = 1;
		if (str[i] == c && !j)
			n++;
		else if (j)
		{
			while (str[i] != '"' && str[i])
				i++;
			if (str[i] == '"')
				j = 0;
			else
				return (0);
		}
		i++;
	}
	return (n);
}

void	ft_cd2(t_env *e, char **tb)
{
	if (!ft_check_cd(tb))
		return ;
	else
	{
		ft_replace_value("OLDPWD", getcwd(NULL, 0), &e);
		chdir(tb[1]);
	}
}

void	ft_cd(char **tb, char *home, t_env *e)
{
	char *path;

	path = NULL;
	if (ft_tablen(tb) == 1)
	{
		ft_replace_value("OLDPWD", getcwd(NULL, 0), &e);
		chdir(home);
	}
	else if (!ft_strcmp(tb[1], "-"))
		chdir(ft_get_value("OLDPWD", e));
	else if (!ft_strcmp(tb[1], "."))
		return ;
	else if (!ft_strcmp(tb[1], ".."))
	{
		path = getcwd(NULL, 0);
		ft_replace_value("OLDPWD", ft_strdup(path), &e);
		ft_remove_last(&path, '/');
		chdir(path);
		free(path);
	}
	else
		ft_cd2(e, tb);
}

void	ft_remove_last(char **s, int c)
{
	int i;

	i = ft_strlen(*s) - 1;
	while (i >= 0 && (*s)[i] != c)
	{
		(*s)[i] = '\0';
		i--;
	}
}
