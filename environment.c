/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   environment.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlamhidr <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/05 21:44:41 by hlamhidr          #+#    #+#             */
/*   Updated: 2019/03/07 17:06:04 by hlamhidr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "jojwahdsh.h"

size_t	ft_lstlen(t_env *list)
{
	size_t i;

	i = 0;
	while (list != NULL)
	{
		i++;
		list = list->next;
	}
	return (i);
}

void	ft_replace_value(char *key, char *value, t_env **e)
{
	while ((*e) != NULL)
	{
		if (!ft_strcmp(key, (*e)->key))
		{
			free((*e)->value);
			(*e)->value = value;
			break ;
		}
		(*e) = (*e)->next;
	}
}

t_env	*ft_lstenvnew(char *key, char *value)
{
	t_env	*new;

	if (!(new = (t_env *)malloc(sizeof(t_env))))
		return (NULL);
	new->next = NULL;
	new->key = key;
	new->value = value;
	return (new);
}

t_env	*add_last(t_env *lst, t_env *new)
{
	t_env *begin;

	begin = lst;
	if (begin == NULL)
		return (new);
	else
	{
		while (begin->next != NULL)
			begin = begin->next;
		begin->next = new;
	}
	return (lst);
}

t_env	*env_management(char **env)
{
	t_env	*new;
	t_env	*e;
	int		j;

	e = NULL;
	new = NULL;
	j = 0;
	while (env[j])
	{
		new = ft_lstenvnew(ft_strsub(env[j], 0, ft_indexofchar(env[j], '=')),
				ft_strdup(ft_strrchr(env[j], '=') + 1));
		e = add_last(e, new);
		j++;
	}
	return (e);
}
