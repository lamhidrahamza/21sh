/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tab_function.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlamhidr <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/05 21:41:27 by hlamhidr          #+#    #+#             */
/*   Updated: 2019/03/07 17:05:12 by hlamhidr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "jojwahdsh.h"

int		ft_tablen(char **tb)
{
	int i;

	i = 0;
	while (tb[i])
		i++;
	return (i);
}

void	ft_catch_signal(int signal)
{
	signal = 0;
	if (!g_sign)
		ft_putstr("\n\033[0;32m21sh $>\033[0m ");
	else
		ft_putchar('\n');
}

void	ft_eddit_tab(char ***tb, char *home, t_env *e)
{
	int		j;
	char	*val;

	val = NULL;
	j = 0;
	while ((*tb)[j])
	{
		if ((*tb)[j][0] == '$')
		{
			val = ft_get_value((*tb)[j] + 1, e);
			if (val)
			{
				free((*tb)[j]);
				(*tb)[j] = ft_strdup(val);
			}
		}
		if ((*tb)[j][0] == '~')
		{
			val = (*tb)[j];
			(*tb)[j] = ft_strjoin(home, (*tb)[j] + 1);
			free(val);
		}
		j++;
	}
}

void	ft_free_tab(char **tb)
{
	int j;

	j = 0;
	while (tb[j])
		free(tb[j++]);
	free(tb);
}

char	**ft_convert_totab(t_env *e)
{
	char	**env;
	char	*tmp;
	int		i;

	tmp = NULL;
	i = 0;
	if (!(env = (char **)malloc(sizeof(char *) * ft_lstlen(e) + 1)))
		return (NULL);
	while (e != NULL)
	{
		tmp = ft_strjoin(e->key, "=");
		env[i] = ft_strjoin(tmp, e->value);
		free(tmp);
		e = e->next;
		i++;
	}
	env[i] = 0;
	return (env);
}
