/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_and_split.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlamhidr <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/05 21:36:54 by hlamhidr          #+#    #+#             */
/*   Updated: 2019/03/07 16:51:15 by hlamhidr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "jojwahdsh.h"

void	ft_remove_lst(t_env *e)
{
	t_env *last;

	while (e != NULL)
	{
		free(e->key);
		free(e->value);
		last = e;
		e = e->next;
		free(last);
	}
}

char	*ft_strsub_and_free_s(char *s, unsigned int start, size_t len)
{
	char	*fr;
	size_t	i;

	if (!s)
		return (NULL);
	if (!(fr = (char *)malloc(sizeof(char) * len + 1)))
		return (NULL);
	i = 0;
	while (len > i)
		fr[i++] = s[start++];
	fr[i] = '\0';
	free(s);
	return (fr);
}


void	ft_exit_and_free(char **tb, char *path, char *home, t_env *e)
{
	free(home);
	ft_remove_lst(e);
	ft_free_tab(tb);
	free(path);
	ft_putstr("exit\n");
	exit(0);
}

char	**ft_split_command(char *line)
{
	char	**tb;
	int		i;

	i = 0;
	while (line[i])
	{
		if (line[i] == '"')
		{
			line[i] = -1;
			i++;
			while (line[i] != '"')
				i++;
			line[i] = -1;
		}
		if (line[i] == ' ' || line[i] == '\t')
			line[i] = -1;
		i++;
	}
	tb = ft_strsplit(line, -1);
	free(line);
	return (tb);
}

int		my_outc(int c)
{
	ft_putchar(c);
	return (1);
}

int		ft_get_num_of_lines(int num_col, char *s)
{
	int num_lines;
	int i;
	int j;

	i = 0;
	num_lines = 1;
	j = 8;
	while (s[i])
	{
		if (j == num_col - 1 || s[i] == '\n')
		{	
			num_lines++;
			j = 0;
		}
		else
			j++;
		i++;
	}
	return (num_lines);
}

int		ft_get_num_of_lines_add(int len, int num_col)
{
	int num_lines;

	num_lines = (len + 8) / num_col;
	if ((len + 8) % num_col > 0)
		num_lines++;
	return (num_lines);
}

void	ft_move_left(int n)
{
	while (n--)
		tputs(tgetstr("le", NULL), 0, my_outc);
}

void	ft_move_right(int n)
{
	while (n--)
		tputs(tgetstr("nd", NULL), 0, my_outc);
}

void	ft_movecur_up_and_right(int up, int size)
{
	while (up--)
		tputs(tgetstr("up", NULL), 0, my_outc);
	while (size--)
		tputs(tgetstr("nd", NULL), 0, my_outc);
}

void	ft_remove_selections(t_cursor *pos, t_select *select, char *s)
{
	int		num_col;
	int		num_lines;

	num_col = ft_get_size_windz();
	num_lines = ft_get_num_of_lines(num_col, s);
	ft_move_cursor_zero(*pos);
	tputs(tgetstr("cd", NULL), 0, my_outc);
	ft_putstr(s);
	select->start = -1;
	select->end = -1;
	ft_set_last_position(*pos, num_lines, num_col);
}

void	ft_see_touch(char t, char *s, t_cursor *pos, t_select *select)
{
	int		size;

	size = ft_strlen(s);
	if (select->end != -1 && select->start -1)
		ft_remove_selections(pos, select, s);
	if (t == 'D' && pos->x == 0)
	{
		pos->index--;
		pos->y--;
		pos->x = pos->end[pos->y];
		ft_movecur_up_and_right(1, pos->x);
	}
	else if (t == 'D' && pos->index != 0)
	{
		pos->index--;
		pos->x--;
		tputs(tgetstr("le", NULL), 0, my_outc);
	}
	else if (t == 'C' && pos->x == pos->end[pos->y] && pos->index < size)
	{
		pos->index++;
		pos->x = 0;
		pos->y++;
		tputs(tgetstr("do", NULL), 0, my_outc);
	}
	else if (t == 'C' && pos->index < size)
	{
		tputs(tgetstr("nd", NULL), 0, my_outc);
		pos->index++;
		pos->x++;
	}
}

void	ft_get_end_of_line_pos(t_cursor *pos, char *s, int num_col)
{
	int i;
	int x;
	int y;

	i = 0;
	x = 8;
	y = 0;
	ft_mmmm(&pos->end);
	while (s[i])
	{
		if (x == num_col - 1 || s[i] == '\n')
		{
			pos->end[y] = x;
			x = 0;
			y++;

		}
		else
			x++;
		i++;
	}
	pos->end[y] = x;
}

void	ft_move_cursor_fordel(t_cursor *pos, int num_col)
{
	if (pos->x == 0)
	{
		pos->y--;
		pos->x = pos->end[pos->y];
		ft_movecur_up_and_right(1, pos->x);
	}
	else
	{
		tputs(tgetstr("le", NULL), 0, my_outc);
		pos->x--;
	}
	tputs(tgetstr("cd", NULL), 0, my_outc);
}

void	ft_putstr_term(int num_col, char *s, t_cursor *pos)
{
	int i;
	int x;

	i = 0;
	x = pos->x;
	while (s[i])
	{
		ft_putchar(s[i]);
		if (x == num_col - 1 || s[i] == '\n')
		{
			if (x == num_col - 1 && s[i] != '\n')
				tputs(tgetstr("do", NULL), 0, my_outc);
			x = 0;
		}
		else
			x++;
		i++;
	}
}

void	ft_set_last_position(t_cursor pos, int num_lines, int num_col)
{
	int y;
	int real_pos;
	
	y = num_lines - 1;
	real_pos = pos.end[num_lines - 1];
	while (pos.y < y--)
		tputs(tgetstr("up", NULL), 0, my_outc);
	if (real_pos > pos.x)
		while (real_pos-- > pos.x)
			tputs(tgetstr("le", NULL), 0, my_outc);
	else if (real_pos < pos.x)
		while (real_pos++ < pos.x)
			tputs(tgetstr("nd", NULL), 0, my_outc);
}

void	ft_delcolomn(char **s, t_cursor *pos)
{
	int len;
	int num_col;
	int num_lines;

	len = ft_strlen(*s);
	num_col = ft_get_size_windz();
	if (pos->index <= len && pos->index > 0)
	{
		ft_memmove(*s + pos->index - 1 , *s + pos->index, len);
		ft_move_cursor_fordel(pos, num_col);
		ft_putstr_term(num_col, *s + pos->index - 1, pos);
		num_lines = ft_get_num_of_lines(num_col, *s);
		ft_get_end_of_line_pos(pos, *s, num_col);
		ft_set_last_position(*pos, num_lines, num_col);
		pos->index--;
	}
}

void	ft_set_last_position_for_add(t_cursor pos, int real_pos, int num_lines, int num_col)
{
	fprintf(pos.fd, "111 pos.x ==  %d  pos.y == %d  real_pos == %d  num_lines == %d num_col == %d\n\n", pos.x, pos.y, real_pos, num_lines,num_col);
	while (pos.y + 1 < num_lines--)
		tputs(tgetstr("up", NULL), 0, my_outc);
	if (real_pos == 0)
	{	
		tputs(tgetstr("up", NULL), 0, my_outc);
		while (real_pos++ < pos.x)
			tputs(tgetstr("nd", NULL), 0, my_outc);
	}
	else if (real_pos > pos.x)
		while (real_pos-- > pos.x)
			tputs(tgetstr("le", NULL), 0, my_outc);
	else if (real_pos < pos.x)
		while (real_pos++ < pos.x)
			tputs(tgetstr("nd", NULL), 0, my_outc);
}

char	*ft_line_edd(char *s, t_cursor *pos, char c)
{
	char *new;
	int num_col;
	int num_lines;
	int real_pos;

	num_col = ft_get_size_windz();
	new = (char *)malloc(sizeof(char) *  ft_strlen(s) + 2);
	ft_strncpy(new, s, pos->index);
	new[pos->index] = c;
	ft_strcpy(new + pos->index + 1, s + pos->index);
	tputs(tgetstr("cd", NULL), 0, my_outc);
	ft_putstr_term(num_col, new + pos->index, pos);
	pos->index++;
	num_lines = ft_get_num_of_lines(num_col, new);
	if (pos->x == num_col - 1)
	{	
		pos->x = 0;
		pos->y++;
	}
	else
		pos->x++;
	ft_get_end_of_line_pos(pos, new, num_col);
	fprintf(pos->fd, "pos->x == %d  pos->y == %d  pos->index == %d real_pos == %d num_col == %d num_lines == %d\n", pos->x, pos->y, pos->index, pos->end[num_lines - 1], num_col, num_lines);
	ft_set_last_position(*pos, num_lines, num_col);
	return (new);
}

int		ft_get_size_windz(void)
{
	struct winsize ws;
	ioctl(0, TIOCGWINSZ, &ws);
	return (ws.ws_col);
}

void	ft_putline(char c, int len, char **s, t_cursor *pos)
{
	char *tmp;
	int	num_col;
	char buff[2];

	num_col = ft_get_size_windz();
	buff[0] = c;
	buff[1] = '\0';
	ft_putchar(buff[0]);
	if (pos->x == num_col - 1 || c == '\n')
	{
		pos->end[pos->y] = pos->x;
		if (c != '\n')
			tputs(tgetstr("do", NULL), 0, my_outc);
		pos->x = 0;
		pos->y++;
	}
	else
	{
		pos->x++;
		pos->end[pos->y] = pos->x;
	}
	//tmp = *s;
	*s = ft_strjoin(*s, buff);
	//free(tmp);
	pos->index++;
}

void	ft_move_cursor_zero(t_cursor pos)
{
	ft_move_left(pos.x);
	while (pos.y-- > 0)
		tputs(tgetstr("up", NULL), 0, my_outc);
	ft_move_right(8);
}

char *ft_print_history(char **history, int *his_count, char *buf, char *s, t_cursor *pos)
{
	int num_col;
	int num_lines;
	int len;

	len = ft_strlen(s);
	num_col = ft_get_size_windz();
	ft_move_cursor_zero(*pos);
	tputs(tgetstr("cd", NULL), 0, my_outc);
	fprintf(pos->fd, "his_count before %d \n", *his_count);
	if (UP(buf))
	{	
		(*his_count)--;
		ft_stock_history(history, s, *his_count + 1);
	}
	else
		(*his_count)++;
	fprintf(pos->fd, "his_count %d   history |%s|  s == %s\n", *his_count, history[*his_count], history[*his_count + 1]);
	ft_putstr(history[*his_count]);
	ft_get_end_of_line_pos(pos, history[*his_count], num_col);
	num_lines = ft_get_num_of_lines(num_col, history[*his_count]);
	pos->index = ft_strlen(history[*his_count]);
	pos->x = pos->end[num_lines - 1];
	pos->y = num_lines - 1;
	s = ft_strdup(history[*his_count]);
	return (s);
}

void	ft_mmmm(int **d)
{
	int i;

	i = 0;
	while (i < 20)
		(*d)[i++] = -1;
}

char	*ft_read_line(char **history, int his_count, t_select *select)
{
	char	buf[6];
	t_cursor pos;
	char	*s;
	int		i;
	char	*tmp;
	int		len;

	s = ft_strnew(0);
	pos.index = 0;
	pos.x = 8;
	pos.y = 0;
	pos.end = (int *)malloc(sizeof(int) * 20);
	ft_mmmm(&pos.end);
	pos.fd = fopen("/dev/ttys001", "a+");
	if (his_count != 0)
		fprintf(pos.fd, "his_count %d  history == %s\n", his_count - 1, history[his_count - 1]);
	ft_bzero(buf, 6);
	while ((len = read(0, buf, 6)))
	{
		fprintf(pos.fd, "buf[0] == |%d| buf[1] == |%d| buf[2] == |%d| buf[3] == |%d| buf[4] == |%d| buf[5] == |%d|\n", buf[0], buf[1], buf[2], buf[3], buf[4], buf[5]);
		if (buf[0] == '\n' && buf[1] == '\0' && buf[2] == '\0')
		{
			if (select->start != -1 && select->end != -1)
				ft_remove_selections(&pos, select, s);
			ft_putchar('\n');
			break ;
		}
		else if (C_UP(buf) || C_DO(buf))
			ft_move_by_lines(&pos, s, buf);
		else if (HOME(buf) || END(buf))
			ft_home_end(&pos, s, buf);
		else if (RI_WOR(buf) || LE_WOR(buf))
			ft_move_by_word(&pos, s, buf);
		else if (buf[0] == 127)
			ft_delcolomn(&s, &pos);
		else if ((UP(buf) && his_count != 0) || (DO(buf) && history[his_count + 1]))
			s = ft_print_history(history, &his_count, buf, s, &pos);
		else if (buf[0] == 27)
			ft_see_touch(buf[2], s, &pos, select);
		else if (SEL_RI(buf) || SEL_LE(buf))
			ft_selection(s, &pos, buf, select);
		else if (COPY(buf) || PASTE(buf) || CUT(buf))
			ft_copy_paste(buf, &s, &pos, select);	
		else if ((size_t)pos.index != ft_strlen(s))
			s = ft_line_edd(s, &pos, buf[0]);
		else
		{	
			i = 0;
			while ((ft_isprint(buf[i]) || buf[i] =='\n') && i < 6)
			{	
				if (buf[i] != '\t')
					ft_putline(buf[i], len, &s, &pos);
				i++;
			}
		}
		ft_bzero(buf, 6);
	}
	tputs(tgetstr("ae", NULL), 0, my_outc);
	tputs(tgetstr("me", NULL), 0, my_outc);
	//tmp = s;
	s = ft_strtrim(s);
	//free(tmp);
	return (s);
}

