/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   command_functions.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlamhidr <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/05 21:33:54 by hlamhidr          #+#    #+#             */
/*   Updated: 2019/03/07 23:19:42 by hlamhidr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "jojwahdsh.h"

void	ft_putenv(t_env *e)
{
	while (e != NULL)
	{
		ft_putstr(e->key);
		ft_putchar('=');
		ft_putstr(e->value);
		ft_putchar('\n');
		e = e->next;
	}
}

void	ft_echo(char **tb)
{
	int j;
	int i;

	j = 1;
	i = 1;
	while (tb[j])
	{
		if (tb[j][0] == '\'')
		{
			while (tb[j][i] != '\'' && tb[j][i])
				write(1, &tb[j][i++], 1);
			ft_putchar(' ');
		}
		else
		{
			ft_putstr(tb[j]);
			ft_putchar(' ');
		}
		j++;
	}
	ft_putchar('\n');
}

t_env	*ft_unsetenv(char **tb, t_env *e)
{
	t_env *tmp;
	t_env *lst;

	lst = e;
	tmp = NULL;
	if (ft_tablen(tb) == 1)
		ft_putstr("unsetenv: Too few arguments.\n");
	else
		while (lst != NULL)
		{
			if (!ft_strcmp(tb[1], lst->key))
			{
				tmp->next = lst->next;
				free(lst->key);
				free(lst->value);
				free(lst);
			}
			tmp = lst;
			lst = lst->next;
		}
	return (e);
}

void	ft_setenv(char **tb, t_env *e)
{
	t_env	*new;
	int		i;

	i = ft_tablen(tb);
	if (i == 1)
		ft_putenv(e);
	else if (i > 3)
		ft_putstr("setenv: Too many arguments.\n");
	else if (ft_get_value(tb[1], e))
	{
		if (i == 2)
			ft_replace_value(ft_strdup(tb[1]), ft_strdup(""), &e);
		else
			ft_replace_value(ft_strdup(tb[1]), ft_strdup(tb[2]), &e);
	}
	else
	{
		if (i == 2)
			new = ft_lstenvnew(ft_strdup(tb[1]), ft_strdup(""));
		else
			new = ft_lstenvnew(ft_strdup(tb[1]), ft_strdup(tb[2]));
		add_last(e, new);
	}
}

void	ft_callincenter(char **tb, char *path, char *home, t_env *e)
{
	if (!ft_strcmp(tb[0], "exit"))
		ft_exit_and_free(tb, path, home, e);
	if (!ft_strcmp(tb[0], "env"))
		ft_putenv(e);
	else if (!ft_strcmp(tb[0], "setenv"))
		ft_setenv(tb, e);
	else if (!ft_strcmp(tb[0], "unsetenv"))
		ft_unsetenv(tb, e);
	else if (!ft_strcmp(tb[0], "echo"))
		ft_echo(tb);
	else if (!ft_strcmp(tb[0], "cd"))
		ft_cd(tb, home, e);
	else
		ft_minishell_exec(tb, path, e);
	ft_free_tab(tb);
	free(path);
}