/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   selection.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlamhidr <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/24 21:56:04 by hlamhidr          #+#    #+#             */
/*   Updated: 2019/04/24 21:56:07 by hlamhidr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "jojwahdsh.h"

void	ft_print_with_reverse_mode(char *s, int start, int end, int num_col, t_cursor *pos)
{
	int x;

	x = pos->x;
	while (s[start])
	{
		if (start <= end)
		{
			ft_putstr("\033[0;32m");
			tputs(tgetstr("mr", NULL), 0, my_outc);
			ft_putchar(s[start]);
			tputs(tgetstr("me", NULL), 0, my_outc);
			ft_putstr("\033[0m");
		}
		else
			ft_putchar(s[start]);
		if (x == num_col - 1 || s[start] == '\n')
			x = 0;
		else
			x++;
		start++;
	}
}

void	ft_left_selection(char *s, t_cursor *pos, char *key, t_select *select)
{
	int len;
	int up;
	int num_col;
	int num_lines;
	int	real_pos;

	up = 0;
	len = ft_strlen(s);
	num_col = ft_get_size_windz();
	num_lines = ft_get_num_of_lines(num_col, s);
	real_pos = pos->end[num_lines - 1];
	if (pos->index <= len && pos->index >= 0)
	{
		tputs(tgetstr("cd", NULL), 0, my_outc);
		if (pos->x == 0 && pos->index != 0)
		{
			up = 1;
			pos->y--;
			pos->x = pos->end[pos->y];
		}
		else if (pos->index != 0)
			pos->x--;
		if (select->end == -1 && select->start == -1)
		{
			if (pos->index != 0)
			{
				select->start = pos->index;
				select->end = pos->index;
				ft_print_with_reverse_mode(s , select->start, select->end, num_col, pos);
			}
			else
				ft_putstr_term(num_col, s, pos);
		}
		else if ((select->start < select->end) || ((select->start == select->end) && (pos->index < len - 1 && pos->index >= select->end)))
		{	
			select->end--;
			if (up)
				ft_movecur_up_and_right(1, num_col);
			else
				tputs(tgetstr("le", NULL), 0, my_outc);
			tputs(tgetstr("cd", NULL), 0, my_outc);
			ft_putstr_term(num_col, s + select->end + 1, pos);
			if ((select->start == select->end + 1) && (pos->index < len - 1 && pos->index > select->end + 1))
			{
				select->start = -1;
				select->end = -1;
			}
		}
		else if ((select->start > select->end) || ((select->start == select->end) && (pos->index == len - 1 || pos->index < select->end)))
		{	
			if (select->end != 0)
				select->end--;
			ft_print_with_reverse_mode(s ,select->end, select->start, num_col, pos);
		}
		ft_set_last_position(*pos, num_lines, num_col);
		if (pos->index != 0)
			pos->index--;
		fprintf(pos->fd, "start == %d  end == %d real_pos == %d pos->x == %d pos->y == %d pos->index == %d\n", select->start, select->end, real_pos, pos->x, pos->y, pos->index);
	}
}

void    ft_selection(char *s, t_cursor *pos, char *key, t_select *select)
{
    int num_col;
	int num_lines;
	int real_pos;
    int len;
	int down;

	down = 0;
    len = ft_strlen(s);
	num_col = ft_get_size_windz();
	num_lines = ft_get_num_of_lines(num_col, s);
	real_pos = pos->end[num_lines - 1];
    if (SEL_RI(key) && pos->index < len)
	{
		if (pos->x == pos->end[pos->y])
		{
			down = 1;
			pos->x = 0;
			pos->y++;
		}
		else
			pos->x++;
		if (select->end == -1 && select->start == -1)
		{
			select->start = pos->index;
			select->end = pos->index;
			ft_print_with_reverse_mode(s ,pos->index, pos->index, num_col, pos);
			pos->index++;	
		}
		else if (select->start < select->end || (select->start == select->end && pos->index > select->end))
		{	
				select->end++;
				ft_print_with_reverse_mode(s ,pos->index, pos->index, num_col, pos);
				pos->index++;
		}
		else if (select->start > select->end || (select->start == select->end && pos->index < select->end))
		{	
			if (select->end != 0)
			{
				pos->index++;
				if (down)
					tputs(tgetstr("do", NULL), 0, my_outc);
				else
					tputs(tgetstr("nd", NULL), 0, my_outc);
			}
			else
				pos->x--;
			tputs(tgetstr("cd", NULL), 0, my_outc);
			select->end++;
			ft_putchar(s[pos->index]);
			ft_print_with_reverse_mode(s ,select->end, select->start, num_col, pos);
			if (select->end - 1 == select->start)
			{
				select->start = -1;
				select->end = -1;
			}
		}
		ft_set_last_position(*pos, num_lines, num_col);
		fprintf(pos->fd, "start == %d  end == %d real_pos == %d pos->x == %d  pos->y == %d pos->index == %d \n", select->start, select->end, real_pos, pos->x, pos->y, pos->index);
	}
	else if (SEL_LE(key) && pos->index >= 0)
		ft_left_selection(s, pos, key, select);
}

