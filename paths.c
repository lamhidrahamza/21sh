/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   paths.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlamhidr <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/05 21:48:39 by hlamhidr          #+#    #+#             */
/*   Updated: 2019/03/07 17:07:36 by hlamhidr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "jojwahdsh.h"

char	*ft_join_three(char *s1, char *s2, char *s3)
{
	char *to_del;
	char *ret;

	to_del = ft_strjoin(s1, s2);
	ret = ft_strjoin(to_del, s3);
	free(to_del);
	return (ret);
}

char	*ft_get_value(char *variable, t_env *e)
{
	while (e != NULL)
	{
		if (!ft_strcmp(variable, e->key))
			return (e->value);
		e = e->next;
	}
	return (NULL);
}

char	**ft_get_paths(t_env *e)
{
	char **paths;

	while (e != NULL)
	{
		if (!ft_strcmp(e->key, "PATH"))
			break ;
		e = e->next;
	}
	if (!e)
		return (NULL);
	paths = ft_strsplit(e->value, ':');
	return (paths);
}

char	*ft_find_command_path2(char *command, char **paths, DIR *dir, int j)
{
	char	*ret;

	ret = ft_join_three(paths[j], "/", command);
	ft_free_tab(paths);
	closedir(dir);
	return (ret);
}

char	*ft_find_command_path(char *command, t_env *e, int j)
{
	DIR				*dir;
	struct dirent	*dp;
	char			**paths;

	if ((paths = ft_get_paths(e)))
	{
		while (paths[j])
		{
			dir = opendir(paths[j]);
			while ((dp = readdir(dir)) != NULL)
			{
				if (!ft_strcmp(command, dp->d_name))
					return (ft_find_command_path2(command, paths, dir, j));
			}
			closedir(dir);
			j++;
		}
		ft_free_tab(paths);
	}
	return (NULL);
}
